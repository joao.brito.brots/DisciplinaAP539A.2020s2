#' Carrega arquivo CSV
#'
#' @param arquivoCsv 
#'
#' @return
#' @export
#'
#' @examples
CarregarDadosCsv <- function(arquivoCsv, separador = ";"){
 
  # Arquivo csv
  arquivoCsv <- str_c("Data/", arquivoCsv, ".csv")

  # Carrega dados
  dados <- fread(
    file = arquivoCsv,
    sep = separador,
    header = TRUE,
    na.strings = "",
    stringsAsFactors = FALSE,
    verbose = FALSE,
    encoding = "UTF-8",
    strip.white = TRUE,
    fill = TRUE,
    nThread = detectCores() - 1L
  ) %>% 
    as_tibble()
    
  
  # Retorno da funcao
  return(dados) 
}
####
## Fim
#