#' Detecta Encoding de um arquivo
#'
#' @param arquivo 
#'
#' @return encoding do arquivo
#' @export
#'
#' @examples
DetectEncoding <- function(arquivo){
  
  # Tipo de codificacao
  encoding <- rawToChar(
    x = readBin(
      arquivo, 
      "raw",
      1e+06
    )
  ) %>% 
    stri_enc_detect(
      filter_angle_brackets = FALSE
    ) %>% 
    bind_rows() %>% 
    slice(1) %>% 
    pull(Encoding)

  # Retorno da funcao
  return(encoding)
}
####
## Fim
#