#' Modela previsao com metodo GMDH Combinatorial
#'
#' @param metodo Metodo para criar o modelo
#' @param planoExecucao Planos de execucao para criacao dos modelos
#' @param serieTemporal Serie temporal
#'
#' @return Modelo de previsao GMDH Combinatorial
#' @export
#'
#' @examples
ModelarPrevisao_GMDH_COMBI <- function(metodo, serieTemporal, planoExecucao){
  
  # Serue temporal de treino
  serieTemporalTreino <- serieTemporal %>% 
    filter(
      (Periodo >= planoExecucao$TreinoIni),
      (Periodo <= planoExecucao$TreinoFim)
    )
  
  # Cria modelo arima
  modelo <- try(
    expr = gmdh.combi(
      X = serieTemporalTreino %>% 
        select(Periodo, Causal) %>% 
        as.matrix(),
      y = serieTemporalTreino$Serie,
      G = 2,
      criteria = "PRESS"
    ),
    silent = TRUE
  )
  
  # Se houve erro na execucao do modelo
  if(Erro(modelo)){
    return(modelo)
  }
  
  # Base de teste
  serieTemporalTeste$Serie <- serieTemporal %>% 
    filter(
      (Periodo >= planoExecucao$TesteIni),
      (Periodo <= planoExecucao$TesteFim)
    )
  
  # Predicao
  predicao <- predict(
    object = modelo,
    newdata = serieTemporalTeste$Serie %>%  
      select(Periodo, Causal) %>% 
      as.matrix()
  ) %>% 
    as.numeric()
  
  # Analise do modelo
  modelo$Analise <- list(
    Metodo = metodo,
    IdPlanoExecucao = planoExecucao$Id,
    PrevisaoMedia = predicao,
    MAPE_mean = mean(abs(predicao - serieTemporalTeste$Serie) / serieTemporalTeste$Serie) * 100,
    MAPE_median = median(abs(predicao - serieTemporalTeste$Serie) / serieTemporalTeste$Serie) * 100,
    MAPE_sd = sd(abs(predicao - serieTemporalTeste$Serie) / serieTemporalTeste$Serie),
    Grafico = tibble(
      Periodo = planoExecucao$TreinoIni:planoExecucao$TesteFim,
      Media = c(serieTemporalTreino$Serie, predicao)
    ) %>% 
      ggplot(
        aes(
          x = Periodo,
          y = Media
        ) 
      ) +
      geom_line() + 
      scale_x_continuous(
        breaks = planoExecucao$TreinoIni:planoExecucao$TesteFim
      ) +
      xlab(
        label = str_c(planoExecucao$Periodo, "s")
      ) +
      ylab(
        label = "Quantidades"
      ) +
      ggtitle(
        label = str_c(
          "[ ", metodo, " ] ",
          planoExecucao$Base, " ",
          planoExecucao$Serie 
        )
      )
  )
  
  # Retorno da funcao
  return(modelo)
}
####
## Fim
#