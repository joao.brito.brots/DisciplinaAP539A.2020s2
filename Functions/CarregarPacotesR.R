#' Carregar e instala pacotes R
#'
#' @param pacotes Nome dos pacotes R
#'
#' @return
#' @export
#'
#' @examples
CarregarPacotesR <- function(pacotes) {
  
  # Pacotes R
  for(pacote in pacotes){
    
    # Se nao possui o pacote instalado
    if(!require(pacote, character.only = TRUE, quietly = TRUE, warn.conflicts = FALSE)){
      # Instala pacote
      install.packages(
        pkgs = pacote,
        dependencies = c("Depends", "Suggests"),
        quiet = TRUE,
        type = "binary",
        clean = TRUE,
        repos = "https://cran.rstudio.com"
      )
      
      # Carrega pacote
      require(
        package = pacote, 
        character.only = TRUE, 
        quietly = TRUE, 
        warn.conflicts = FALSE
      )
    }
  }
  
  # Lista de pacotes carregados
  pacotesCarregados <- pacotes
  
  # Retorno da funcao
  return(pacotesCarregados)
}
####
## Fim
#