#' Buscar objeto do Cache
#'
#' @param arquivoCache Arquivo para armazenar o cache
#'
#' @return Cache de dados
#' @export
#'
#' @examples
BuscarCache <- function(arquivoCache){
  
  # Se arquivo NAO existe
  if(!PossuiCache(arquivoCache)){
    stop(bold(red("Nao existe arquivo de cache!")))
  }
  
  # Busca objeto
  objeto <- read_rds(
    file = str_c("Cache/", arquivoCache, ".rds")
  )
  
  # Retorno da funcao
  return(objeto)
}
####
## Fim
#