#' Gera estatisticas descritivas
#'
#' @param dados 
#'
#' @return
#' @export
#'
#' @examples
GerarEstatisticasDescritivas <- function(dados){
  
  # Se for tipo numerico
  descritivas <- dados %>% 
    select_if(is.numeric) %>% 
    gather(
      key = "Campo",
      value = "Valores"
    ) %>% 
    group_by(Campo) %>% 
    summarise(
      Media = mean(Valores),
      Mediana = median(Valores),
      DesvioPadrao = sd(Valores),
      CoefVariacao = CoefVariacao(Valores),
      Minimo = min(Valores), 
      Maximo = max(Valores),
    ) %>% 
    ungroup()
  
  # Retorno da funcao
  return(descritivas) 
}
####
## Fim
#